int encoders[3][3] = {
  {
    3,
    4,
    5
  },
  {
    6,
    7,
    8
  },
  {
    9,
    10,
    11
  }
};

static int numbers[3] = {
  0,
  0,
  0
};

void encoderConstructor() {
  for(int i=0; i < sizeof(encoders); i++) {
    for(int j=0; j < sizeof(encoders[i]); j++) {
      pinMode(encoders[i][j], INPUT);
    }
  }
}

int * readEncoders() {
  for(int i=0; i < sizeof(encoders); i++) {
    for(int j=0; j < sizeof(encoders[i]); j++) {
      if(digitalRead(encoders[i][j])) {
        numbers[i] = j;
      }
    }
  }

  return numbers;
}


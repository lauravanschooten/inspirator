
#define TEXT_FILE "strings.txt"
#define TOOL_FILE "tools.txt"

String toolArray[4] = {
  "3D printer",
  "Lasersnijder",
  "CNC Frees",
  "Vinylsnijder"
};

String ideaArray[10][10] = {
  {
    "een storyboard over een vlog in een hypermoderne stad.",
    "Huis"
  },
  {
    "vogelhuisje met de lasersnijder",
    "verlicht Huis"
  },
  {
    "parkour op de grond en laat de bluebot \n dit parkour afleggen",
    "verlicht smart huis"
  }
};

String themes[3] = {
  "Vlog en animatie",
  "Duurzaamheid",
  "Robotica"
};

String difficulties[3] = {
  "Verkennen",
  "Verdiepen",
  "Verbinden"
};

String lines[3][10];
String** text;
String* tools;



void generatorConstructor() {
  // Opening text and tools arrays
  switch(method) {
    // Local Array
    case 1:
      text = getIdeaArray();
      tools = toolArray;
      break;
    // SD Card  
    case 2:
      text = openIdeaFile(TEXT_FILE);
      tools = openTextFile(TOOL_FILE);
      break;
  }
}

String * generate(int numbers[3]) {
  static String strings[2];
  // Return Strings based on positions
  switch(method) {
    case 1:
      strings[0] = ideaArray[numbers[0]][numbers[1]];
      strings[1] = themes[numbers[1]];
      strings[2] = difficulties[numbers[2]];
      break;

    case 2:
      strings[0] = text[numbers[0]][numbers[1]];
      strings[1] = numbers[2];
      break;
  }
  return strings;
}


String ** getIdeaArray() {
  Serial.println("[*]Converting ideaArray.");
  String** returnValue = 0;
  for (int i=0; i < sizeof(ideaArray);i++) {
    //returnValue[i] = new String[sizeof(ideaArray[i])];
    for(int j=0; j < sizeof(ideaArray[i]); j++) {
      //returnValue[i][j] = ideaArray[i][j];
    }
  }
  Serial.println("[*]Returning Array.");
  return returnValue;
}



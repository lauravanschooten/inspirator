#include <Adafruit_Thermal.h>
#include "adalogo.h"

#define PRINTER_BAUD 9600
#define HEADER "header.txt"
#define FOOTER "footer.txt"

Adafruit_Thermal printer(&Serial1);

String* header;
String* footer;

void PrinterConstructor() {
  Serial.println("[*] Starting printer serial.");
  Serial1.begin(PRINTER_BAUD);
  Serial.println("[*] Starting printer.");
  printer.begin();

  switch(method) {
    case 1:
      header[1] = {
        "Makersbase Breda"
      };
      footer[0];
      break;

    case 2:
      header = openTextFile(HEADER);
      footer = openTextFile(FOOTER);  
      break;
      
  }
}

void printIdea(String lines[2], int grade) {
  Serial.println("[*]Starting print");
  printer.justify('C');

  printer.print("Thema:");
  printer.println(lines[1]);

  printer.print("niveau:");
  printer.println(lines[2]);
    
  printer.boldOn();
    printer.println(F("Maak een"));
  printer.boldOff();
  
  printer.underlineOn();
    printer.println("joo");
  printer.underlineOff();
  
  printer.print("\n\n\n\n");

  Serial.println("[*]Done printing.");
}


#define BAUDRATE 9600

boolean triggered = false;
#define STARTPIN 4

// Method determines which source to use
// 1 = local array
// 2 = SD Card
int method = 1;

void setup() {
  // Starting Serial
  Serial.begin(9600);
  Serial.println("[*] Serial Started.");
  
  //Calling Encoder Constructor
  PrinterConstructor();
  encoderConstructor();
  LEDstripConstructor();
  generatorConstructor();
  pinMode(STARTPIN, INPUT);
  Serial.println("[*] Starting Main Loop");
  
}

void loop() {
  if(digitalRead(STARTPIN) >= 1 && !triggered) {
    
    
    int *positions;
    positions = readEncoders();
    String *strings;

    strings = generate(positions);
    printIdea(strings, positions[2]); 
 showProgress(5) ;
    triggered = true;
  } else if(digitalRead(STARTPIN) == 0 && triggered)  {
    triggered = false;
    clearStrip();
  }
}

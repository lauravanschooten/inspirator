#include <SD.h>

const int chipSelect = 0;

struct bitmap {
  int width;
  int height;
  uint8_t data[];
};

void SDCardConstructor() {
  Serial.println("[*] Initializing SD card.");  

  // Start SD card reader
  if (!SD.begin(chipSelect)) {
    Serial.println("[E] Card failed, or not present. exiting.");
    while (1); // Halt
  }
  Serial.println("[*] card initialized.");
}

String** openIdeaFile(String textFile) {
  Serial.println("[*] Opening Text file " + textFile);
  File file = SD.open(textFile);
  String** lines = 0;
  lines = new String*[5];

  int arrayCount = 0;
  int lineCount = 0;
  
  while(file.available()) {
    while(String line = file.readStringUntil("\r")) {
      // If line contains the word 'graad' switch to next array
      // else add line to array
      if(line.indexOf("graad") > 0) {
        lines[arrayCount] = new String[10];
        arrayCount++;
        lineCount = 0;
      } else {
        lines[arrayCount][lineCount] = line;
        lineCount++;
      }
    }
  }
  return lines;
}

String* openTextFile(String filename) {
  Serial.println("[*] Opening text file " + filename);
  // Open file
  File file = SD.open(filename);
  
  String* text = new String[100];
  int count = 0;
  // While there is a string available, add it to the array.
  while(file.available()) {
    while(String line = file.readStringUntil("\r")) {
      text[0] = line;
      count++;
    }
  }
  return text;
}


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIXEL_PIN 12

Adafruit_NeoPixel strip = Adafruit_NeoPixel(48, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

void LEDstripConstructor() {
  Serial.println("[*] Initializing neopixels.");
  pinMode(PIXEL_PIN, OUTPUT);
  strip.begin();
  strip.show();
  Serial.println("[*]Initialized neopixels.");
  
}

void showProgress(int progress) {
  Serial.println("[*]Showing Progress: " + String(progress));
  int pPosition = (progress*8) - 8;
  if(pPosition >= 0) {
   for(int i=0; i < progress; i++) {
    pPosition = (i*8);
    for(int j=0; j < 8; j++) { 
      strip.setPixelColor(pPosition+j, strip.Color(255,0,0));
      strip.show();
    }
    delay(500);
  } 
  }
  Serial.println("[*] Blinking.");
  for(int i=0; i < 4; i++) {
    Serial.println(i);
    for(int x=0; x<=48; x++) {
      strip.setPixelColor(x, strip.Color(0,0,0));
    }
    Serial.println("[*] Setting color");
    strip.show();
    delay(500);
    for(int x=0; x<=48; x++) {
      strip.setPixelColor(x, strip.Color(0,255,0));
    }
    Serial.println("[*] Setting color");
    strip.show();
    delay(500);
  }
}

void clearStrip() {
  for(int x=0; x<=48; x++) {
      strip.setPixelColor(x, strip.Color(0,0,0));
    }
    strip.show();
}
